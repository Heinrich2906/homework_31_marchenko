package com.company;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 30.01.2017.
 */
public class ServerCommon {

    private static final int PORT_NUMBER = 4444;
    static final List<Socket> clients = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT_NUMBER);
            while (true) {
                Socket clientSocket = serverSocket.accept();
                ClientThread clientThread = new ClientThread(clientSocket);
                clientThread.start();
                clients.add(clientSocket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
