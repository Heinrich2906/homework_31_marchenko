package com.company;

import java.io.*;
import java.net.Socket;
import java.rmi.ServerException;
import java.util.Random;

/**
 * Created by heinr on 30.01.2017.
 */
public class ClientThread extends Thread {

    private boolean selectGameMode = false;
    private boolean sucsess = false;
    private boolean flag = true;
    DataOutputStream dos = null;
    DataInputStream dis = null;
    private int myInt;

    private byte countdown = 0;
    private int secret = 0;
    private Socket socket;

    public ClientThread(Socket socket) {
        this.socket = socket;
        try {
            OutputStream outputStream = socket.getOutputStream();
            InputStream inputStream = socket.getInputStream();

            dos = new DataOutputStream(outputStream);
            dis = new DataInputStream(inputStream);

            selectGameMode = true;
            flag = true;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while (flag) {

                System.out.println("common " + flag + " " + selectGameMode);

                if (selectGameMode) {

                    System.out.println("Let's play!");
                    dos.writeUTF("Select game mode: 1 - ordinary; 2 - expert; 0 - exit");
                    System.out.println("Send a weclome message");

                    int myInt = dis.readInt();

                    System.out.println("I'm get myInt " + myInt);

                    switch (myInt) {
                        case 0:
                            selectGameMode = false;
                            dos.writeUTF("exit");
                            flag = false;
                            break;
                        case 1:
                            selectGameMode = false;
                            countdown = 7;
                            secret = new Random().nextInt(99) + 1;
                            break;
                        case 2:
                            selectGameMode = false;
                            countdown = 9;
                            secret = new Random().nextInt(999) + 1;
                            break;
                        default:
                            System.out.println("Full stop!!!");
                            selectGameMode = false;
                            dos.writeUTF("exit");
                            flag = false;
                            break;
                    }
                } else {

                    dos.writeUTF("Number of counts is " + countdown);
                    sucsess = false;

                    for (int i = 0; i < countdown; i++) {

                        System.out.println("In for");

                        myInt = dis.readInt();
                        System.out.println("My int is " + myInt);

                        if (myInt == 0) {
                            dos.writeUTF("Game is over!");
                            selectGameMode = true;
                            flag = false;
                        }
                        if (myInt == secret) {
                            dos.writeUTF("You are guess this number. Would you like play another game? (1 - ordinary; 2 - expert; 0 - exit)");
                            selectGameMode = true;
                            sucsess = true;
                            break;
                        }
                        if (myInt > secret) {
                            dos.writeUTF("The secret number is less");
                        }
                        if (myInt < secret) {
                            dos.writeUTF("The secret number is bigger");
                        }
                    }
                    if (!sucsess) {
                        dos.writeUTF("Number of trying is too big. You lose (((. Would you like play another game? (1 - ordinary; 2 - expert; 0 - exit)");
                        selectGameMode = true;

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            ServerCommon.clients.remove(socket);
            System.out.println("Client disconnected");
        }
    }
}