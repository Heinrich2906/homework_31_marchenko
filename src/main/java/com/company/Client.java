package com.company;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by heinr on 30.01.2017.
 */
public class Client {

    public static void main(String[] args) {

        DataOutputStream dos = null;
        DataInputStream dis = null;
        boolean firstRun = true;
        Socket socket = null;
        boolean flag = true;
        String string = "";

        try {

            socket = new Socket("127.0.0.1", 4444);

            OutputStream outputStream = socket.getOutputStream();
            InputStream inputStream = socket.getInputStream();
            dos = new DataOutputStream(outputStream);
            dis = new DataInputStream(inputStream);

            while (flag) {

                string = dis.readUTF();

                if (string.equals("exit")) {
                    flag = false;
                } else {
                    System.out.println(string);
                    Scanner in = new Scanner(System.in);
                    int myNumber = in.nextInt();
                    dos.writeInt(myNumber);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert dis != null;
            assert dos != null;
            try {
                dis.close();
                dos.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
